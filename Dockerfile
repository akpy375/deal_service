FROM python:3.10.0

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN mkdir /src

WORKDIR /src

COPY poetry.lock pyproject.toml /src/

RUN pip3 install poetry
# We disable virtual environment creation inside the container,
# as it is only needed on the local machine
RUN poetry config virtualenvs.create false

RUN poetry install --no-dev --no-root

COPY ./ /src/

ENTRYPOINT ["sh", "entrypoint.sh"]
