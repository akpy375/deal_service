import os

from rest_framework import serializers

ALLOWED_IMAGE_EXTENSIONS = ("csv",)


def extension(file):
    """Returns the file extension"""
    name, extension = os.path.splitext(file.name)
    return extension.split(".")[-1]


def validate_file_size(temp_file):
    max_size: int = 100  # Mb

    if temp_file.size > max_size * 1024 * 1024:
        raise serializers.ValidationError(
            (f"The file {temp_file} exceeds {max_size} Mb"),
            code="invalid",
        )


def validate_file_type(temp_file):
    if extension(temp_file) not in ALLOWED_IMAGE_EXTENSIONS:
        raise serializers.ValidationError(
            (f"The file {temp_file} must be csv"),
            code="invalid",
        )
