import pandas as pd
from pandas.core.frame import DataFrame
from rest_framework import status
from rest_framework.response import Response

from deal.models import CommonDeal


class CsvSrvice:
    def read_csv(self, file) -> DataFrame:
        try:
            reader = pd.read_csv(file)
        except Exception:
            return Response(
                "File read error",
                status=status.HTTP_400_BAD_REQUEST,
            )
        return reader


class CommonDealService:
    def bulk_create(self, rows):
        try:
            CommonDeal.objects.bulk_create(rows)
        except Exception as ex:
            return Response(
                {"Error:": str(ex)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    def delete(self) -> None:
        CommonDeal.objects.all().delete()

    def prepare_data(self, data) -> list[CommonDeal]:
        return [
            CommonDeal(
                customer=row["customer"],
                item=row["item"],
                total=row["total"],
                quantity=row["quantity"],
                date=row["date"],
            )
            for _, row in data.iterrows()
        ]
