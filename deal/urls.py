from django.urls import path
from rest_framework.routers import SimpleRouter

from deal.views import TopClientView, UploadCsvView

router = SimpleRouter()
urlpatterns = router.urls

urlpatterns = [
    path(
        "upload_csv/",
        UploadCsvView.as_view(),
        name="upload_csv",
    ),
    path(
        "top_client/",
        TopClientView.as_view(),
        name="get_top_client",
    ),
] + router.urls
