from rest_framework import serializers

from deal.models import CommonDeal
from deal.validators import validate_file_size, validate_file_type


class UploadFileSerializer(serializers.Serializer):
    file = serializers.FileField(
        validators=[validate_file_type, validate_file_size],
    )


class DealSerializer(serializers.ModelSerializer):
    spent_money = serializers.IntegerField()
    gems = serializers.ListField()
    username = serializers.CharField(source="customer")

    class Meta:
        model = CommonDeal
        fields = [
            "username",
            "spent_money",
            "gems",
        ]
