from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import F, Sum
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import generics, parsers, status
from rest_framework.request import Request
from rest_framework.response import Response

from deal.models import CommonDeal
from deal.serializers import DealSerializer, UploadFileSerializer
from deal.service import CommonDealService, CsvSrvice


class UploadCsvView(generics.CreateAPIView):
    serializer_class = UploadFileSerializer
    parser_classes = (parsers.MultiPartParser,)

    @method_decorator(cache_page(60 * 60 * 2))
    def create(self, request: Request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file_uploaded = serializer.validated_data["file"]

        csv_service: CsvSrvice = CsvSrvice()
        cd_service: CommonDealService = CommonDealService()

        reader = csv_service.read_csv(file_uploaded)
        deal_rows = cd_service.prepare_data(reader)

        # Removing all records from a table
        cd_service.delete()

        cd_service.bulk_create(deal_rows)
        response = (
            f"The file `{file_uploaded}`"
            f" was uploaded and processed successfully"
        )
        return Response(response, status=status.HTTP_201_CREATED)


class TopClientView(generics.ListAPIView):
    queryset = CommonDeal.objects.all()
    serializer_class = DealSerializer

    def get_queryset(self):
        top_customers = (
            CommonDeal.objects.values("customer")
            .annotate(spent_money=Sum(F("total") * F("quantity")))
            .annotate(gems=ArrayAgg("item", distinct=True))
            .order_by("-spent_money")[:5]
        )
        return top_customers

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response({"response": serializer.data})
