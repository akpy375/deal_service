from django.db import models


class CommonDeal(models.Model):
    """CommonDeal model"""

    customer = models.CharField(max_length=255)
    item = models.CharField(max_length=255)
    total = models.IntegerField()
    quantity = models.IntegerField()
    date = models.DateTimeField()
