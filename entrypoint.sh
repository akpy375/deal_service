poetry run python manage.py migrate
poetry run python manage.py collectstatic --no-input
poetry run python -m gunicorn -b 0.0.0.0:8000  -c "./gunicorn_conf.py" service.wsgi:application
