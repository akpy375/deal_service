# The Deal Service

## Запуск проекта с Poetry

|    command    | description   |
|:--------------|:--------------|
| ```git clone https://gitlab.com/akpy375/deal_service.git``` |  Копируем проект        |
| ```cd deal_service```         | Заходим в папку с проектом        |
| ```poetry shell```         | Создаем и активируем рабочее окружение        |
| ```poetry install```       | Установка зависимостей        |
| ```cp example.env .env``` |  Создаем .env и заполняем своими крэдами   |
| ```python manage.py runserver```       | Запуск проекта        |
| ```pre-commit install```       | Установка pre-commit хуков        |
| ```pre-commit```       | Ручная проверка проблем  |


## Запуск проекта в Docker
```docker compose up --build```

## Документация по адресу в Swagger:
http://0.0.0.0:8000/swagger/
